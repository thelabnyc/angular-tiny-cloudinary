# Angular Tiny Cloudinary

This project makes it easy to use Cloudinary with Angular while keeping your bundle size small.

# Features

- Tiny bundle size, no lodash needed
- Global defaults (for example, auto format and auto quality by default)
- Angular Universal compatible. Renders img tags server side.
- It does not support transformations that require client side code such as responsive widths. This fundamentally wouldn't work with server side rendering. Considering using srcset instead.
- Mostly compatible syntax with cloudinary_angular
- Publishes versions under the same npm name instead of making cloudinary_angular_x. This helps keep up to date on changes.
- Basic video support (may be missing features).
- Alternative, SSR friendly responsive images using srcset

# Install

1. `ng add angular-tiny-cloudinary`
2. Add `TinyCloudinaryModule.forRoot({cloudName: "demo"})` to app.module.ts. Replace demo with your cloudinary name.

# Usage

This project mostly matches the [Angular documentation from cloudinary](https://cloudinary.com/documentation/angular_integration). Use cld as the prefix instead of cl. For example `<cl-image>` becomes `<cld-image>`. Also use camel case instead of hyphens. `default-image` becomes `defaultImage`.

Full example

```
<cl-image public-id="sample.jpg" >
  <cl-transformation background="#00f6ed" default-image="sample" opacity="55">
  </cl-transformation>
</cl-image>
```

becomes

```
<cld-image publicId="sample.jpg" >
  <cld-transformation background="#00f6ed" defaultImage="sample" opacity="55">
  </cld-transformation>
</cld-image>
```

Not all Module.forRoot configuration is supported. Consider adding a merge request to add any features you need. We also support additional configuration options

- cloudName - cloudinary_angular calls this cloud_name and is very inconsistent with formatting. This project always uses camel case.
- defaults - A global default for some transformations. For example, you could apply "f_auto" by default but still override the fetch format as needed.
  - f_auto - Set to true to apply auto fetch format.
  - q_auto - Set to true to apply auto quality.
  - dpr_auto - Set to true to apply auto dpr;
  - defaultImage - Set to any string to make images show a ? if the public ID is not found. Cloudinary documentation says it can be set to the public ID of the default image to use. If you know how to make that work, open a issue.

For more usage examples, see [app.component.html](src/app/app.component.html)

## Responsive images

Cloudinary provides a few ways to implement responsive images. Client hints have poor browser support at this time. Client side JS based solutions mean your images won't even start downloading until JS loads.

We recommend using srcset instead. To help make this easier we introduce cldSrcSet which deviates from the official Cloudinary project. cldSrcSet should be an array of widths in pixels. Only setting widths is supported. One tradeoff of this approach is that DPR support is limited only to browsers that support cross domain client hints (at time of writing, only mobile Chrome). Supporting width and dpr on srcset would result in a very large number of srcsets for every width and dpr combination.

```
  <cld-image
    publicId="balloons"
    fetchFormat="auto"
    crop="scale"
    [cldSrcSet]="[320, 480, 800]"
    sizes="(max-width: 320px) 280px, (max-width: 480px) 440px, 800px"
  ></cld-image>
```

Setting sizes it optional. The img tag will treat the default as "100vw". This prop is passed through to the img sizes attribute.

If you'd like you use dpr_auto or w_auto you should add this header to your page.

`<meta http-equiv="Accept-CH" content="DPR, Viewport-Width, Width">`
