import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { TinyCloudinaryModule } from "projects/angular-tiny-cloudinary/src/public-api";
import { AppComponent } from "./app.component";

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule.withServerTransition({ appId: "serverApp" }),
    TinyCloudinaryModule.forRoot({
      cloudName: "demo",
      defaults: {
        f_auto: true,
        q_auto: true,
        w_auto: true
      }
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
