/*
 * Public API Surface of angular-tiny-cloudinary
 */

export * from "./lib/tiny-cloudinary.service";
export * from "./lib/tiny-cloudinary.module";
export * from "./lib/interfaces";
export * from "./lib/cloudinary-image.component";
export * from "./lib/cloudinary-video.component";
export * from "./lib/cloudinary-transformation.directive";
