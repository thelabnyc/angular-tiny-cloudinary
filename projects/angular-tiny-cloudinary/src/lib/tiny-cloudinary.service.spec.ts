import { TestBed } from "@angular/core/testing";

import { TinyCloudinaryService } from "./tiny-cloudinary.service";
import { TinyCloudinaryModule } from "./tiny-cloudinary.module";

describe("TinyCloudinaryService", () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [TinyCloudinaryModule.forRoot({ cloudName: "demo" })]
    })
  );

  it("should build cloudinary image url", () => {
    const service: TinyCloudinaryService = TestBed.get(TinyCloudinaryService);
    const result = service.buildUrl("sample");
    expect(result).toContain("demo");
  });

  it("should build cloudinary image url with transformations", () => {
    const service: TinyCloudinaryService = TestBed.get(TinyCloudinaryService);
    const result = service.buildUrl("sample", { width: "100" });
    expect(result).toContain("w_100");
  });

  it("should build cloudinary image url with multiple transformations", () => {
    const service: TinyCloudinaryService = TestBed.get(TinyCloudinaryService);
    const result = service.buildUrl("sample", { width: "100" }, [
      { height: "50" }
    ]);
    const width = result.indexOf("w_100");
    const height = result.indexOf("h_50");
    // width and height strings should exist. Width should come first.
    expect(width).toBeGreaterThan(0);
    expect(height).toBeGreaterThan(0);
    expect(width).toBeLessThan(height);
  });
});
