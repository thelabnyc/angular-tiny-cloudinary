import { TestBed, ComponentFixture, async } from "@angular/core/testing";
import { CloudinaryImageComponent } from "./cloudinary-image.component";

describe("CloudinaryImageComponent", () => {
  let component: CloudinaryImageComponent;
  let fixture: ComponentFixture<CloudinaryImageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CloudinaryImageComponent],
      providers: [
        {
          provide: "TinyCloudinaryConfiguration",
          useValue: { cloudName: "demo" },
        }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CloudinaryImageComponent);
    component = fixture.componentInstance;
  });

  it("should exist", () => {
    expect(component).toBeDefined();
  });

  it("should accept props and generate img", async(() => {
    component.border = "5";
    component.publicId = "sample";
    component.alt = "This is a picture"
    fixture.detectChanges();
    const elem = fixture.nativeElement.querySelector("img");
    const src = elem.getAttribute("src");
    const alt = elem.getAttribute("alt");
    expect(src).toContain("bo_5");
    expect(alt).toContain("This is a picture")
  }));
  it("should accept props and generate img with an alt tag IF the alt tag is present", async(() => {
    component.border = "7";
    component.publicId = "Sample two";
    component.alt = undefined
    fixture.detectChanges();
    const elem = fixture.nativeElement.querySelector("img");
    const src = elem.getAttribute("src");
    const alt = elem.getAttribute("alt");
    expect(src).toContain("bo_7");
    expect(alt).toBeNull();
  }));
});
