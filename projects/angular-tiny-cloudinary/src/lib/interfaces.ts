export interface TinyCloudinaryConfigurationDefaults {
  readonly f_auto?: boolean;
  readonly q_auto?: boolean;
  readonly defaultImage?: string;
  readonly dpr_auto?: boolean;
  readonly w_auto?: boolean;
  readonly alt?: string;
}

export interface TinyCloudinaryConfiguration {
  readonly cloudName: string;
  readonly defaults?: TinyCloudinaryConfigurationDefaults;

  /* From angular-cloudinary, but not supported here */
  // readonly upload_preset?: string;
  // readonly api_key?: string;
  // readonly api_secret?: string;
  // readonly cdn_subdomain?: string;
  // readonly cname?: string;
  // readonly private_cdn?: string;
  // readonly protocol?: string;
  // readonly resource_type?: string;
  // readonly responsive_class?: string;
  // readonly responsive_use_breakpoints?: boolean;
  // readonly responsive_width?: string;
  // readonly round_dpr?: true;
  // readonly secure?: boolean;
  // readonly secure_cdn_subdomain?: boolean;
  // readonly secure_distribution?: string;
  // readonly shorten?: boolean;
  // readonly type?: string;
  // readonly url_suffix?: string;
  // readonly use_root_path?: boolean;
  // readonly version?: string;
  // readonly client_hints?: boolean;
}

/**
 * This string should only contain numbers. Doesn't actaully check.
 * Maybe someday Typescript supports regex or something.
 */
export type StringNumber = string;
export type FetchFormat = "auto" | string;
export type Quality =
  | "auto"
  | "auto:best"
  | "auto:good"
  | "auto:aco"
  | "auto:low"
  | "jpegmini"
  | "jpegmini:1"
  | "jpegmini:2"
  | StringNumber;
export type Crop =
  | "scale"
  | "fit"
  | "mfit"
  | "fill"
  | "lfill"
  | "limit"
  | "pad"
  | "lpad"
  | "mpad"
  | "crop"
  | "thumb"
  | "imagga_crop"
  | "imagga_scale";
export type Background = "auto" | string;
export type DPR = "auto" | StringNumber;

export interface Transformation {
  fetchFormat?: FetchFormat;
  quality?: Quality;
  crop?: Crop;
  gravity?: string;
  width?: StringNumber;
  height?: StringNumber;
  radius?: StringNumber;
  angle?: string;
  effect?: string;
  opacity?: StringNumber;
  border?: string;
  background?: Background;
  /** Doesn't seem to work as described in cloudinary's documentation. Setting to anything makes it show a ? */
  defaultImage?: string;
  zoom?: StringNumber;
  aspectRatio?: StringNumber;
  dpr?: DPR;
  color?: string;
  flags?: string | string[];
}
