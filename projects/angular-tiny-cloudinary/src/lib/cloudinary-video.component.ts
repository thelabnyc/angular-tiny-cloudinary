import {
  Component,
  Input,
  ChangeDetectionStrategy,
  ContentChildren,
  QueryList,
  ElementRef,
} from '@angular/core';
import { TinyCloudinaryService } from './tiny-cloudinary.service';
import { CloudinaryTransformationDirective } from './cloudinary-transformation.directive';
import {
  FetchFormat,
  Transformation,
  Quality,
  Crop,
  StringNumber,
  Background,
  DPR,
} from './interfaces';

@Component({
  selector: 'cld-video',
  template: `
    <video
      *ngIf="publicId"
      [poster]="getImageUrl()"
      [controls]="controls"
      [autoplay]="autoplay"
      [loop]="loop"
      [attr.muted]="muted"
      [attr.playsinline]="playsinline"
    >
      <source [src]="getVideoWebm()" type="video/webm" />
      <source [src]="getVideoMp4()" type="video/mp4" />
      <source [src]="getVideoOgv()" type="video/ogg" />
    </video>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CloudinaryVideoComponent {
  @Input() publicId: string;
  @Input() controls: boolean;
  @Input() autoplay: boolean;
  @Input() loop: boolean;
  @Input() muted: boolean;
  @Input() playsinline: boolean;

  @Input() fetchFormat: FetchFormat;
  @Input() width: StringNumber;
  @Input() height: StringNumber;
  @Input() crop: Crop;
  @Input() gravity: string;
  @Input() quality: Quality;
  @Input() radius: StringNumber;
  @Input() angle: string;
  @Input() effect: string;
  @Input() opacity: StringNumber;
  @Input() border: string;
  @Input() background: Background;
  @Input() defaultImage: string;
  @Input() zoom: StringNumber;
  @Input() aspectRatio: StringNumber;
  @Input() dpr: DPR;
  @Input() color: string;
  @Input() flags: string | string[];
  @Input() poster?: string;
  @ContentChildren(CloudinaryTransformationDirective)
  transformations: QueryList<CloudinaryTransformationDirective>;

  constructor(private el: ElementRef, private service: TinyCloudinaryService) {}

  getUrl(format: FetchFormat) {
    const transforms = this.getTransformationsAttrs();
    const componentTransforms: Transformation = {
      fetchFormat: format,
      width: this.width,
      height: this.height,
      crop: this.crop,
      gravity: this.gravity,
      quality: this.quality,
      radius: this.radius,
      angle: this.angle,
      effect: this.effect,
      opacity: this.opacity,
      border: this.border,
      background: this.background,
      defaultImage: this.defaultImage,
      zoom: this.zoom,
      aspectRatio: this.aspectRatio,
      dpr: this.dpr,
      color: this.color,
      flags: this.flags,
    };
    return this.service.buildUrl(
      this.publicId,
      componentTransforms,
      transforms,
      true,
    );
  }

  getImageUrl() {
    if (this.poster) {
      const transforms = this.getTransformationsAttrs();
      const componentTransforms: Transformation = {
        fetchFormat: 'jpg',
        width: this.width,
        height: this.height,
        crop: this.crop,
        gravity: this.gravity,
        quality: this.quality,
        radius: this.radius,
        angle: this.angle,
        effect: this.effect,
        opacity: this.opacity,
        border: this.border,
        background: this.background,
        defaultImage: this.defaultImage,
        zoom: this.zoom,
        aspectRatio: this.aspectRatio,
        dpr: this.dpr,
        color: this.color,
        flags: this.flags,
      };
      return this.service.buildUrl(
        this.poster,
        componentTransforms,
        transforms,
        false,
      );
    } else {
      return this.getUrl('jpg');
    }
  }

  getVideoWebm() {
    return this.getUrl('webm');
  }

  getVideoMp4() {
    return this.getUrl('mp4');
  }

  getVideoOgv() {
    return this.getUrl('ogv');
  }

  private getTransformationsAttrs(): Transformation[] {
    const attrNodes = this.transformations.map((transformation) =>
      transformation.getAttributes(),
    );
    const transforms = attrNodes
      .map((attrNode) => {
        const numAttrs = attrNode.length;
        const result: Attr[] = [];
        for (let i = 1; i < numAttrs; i++) {
          result.push(attrNode.item(i));
        }
        return result;
      })
      .map((attr) => attr.map((a) => ({ key: a.name, value: a.value })));

    return transforms.map((transform) => {
      const result: Transformation = {};
      transform.map((item) => {
        result[item.key] = item.value;
      });
      return result;
    });
  }
}
