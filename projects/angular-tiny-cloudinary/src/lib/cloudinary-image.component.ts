import {
  Component,
  Input,
  ChangeDetectionStrategy,
  ContentChildren,
  QueryList,
  ElementRef,
} from '@angular/core';
import { TinyCloudinaryService } from './tiny-cloudinary.service';
import { CloudinaryTransformationDirective } from './cloudinary-transformation.directive';
import {
  FetchFormat,
  Transformation,
  Quality,
  Crop,
  StringNumber,
  Background,
  DPR,
} from './interfaces';

@Component({
  selector: 'cld-image',
  template: `
    <img
      *ngIf="publicId"
      [src]="getUrl()"
      [attr.alt]="alt"
      [attr.sizes]="sizes ? sizes : null"
      [attr.srcset]="cldSrcSet ? getSrcSet() : null"
      [attr.loading]="loading"
    />
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CloudinaryImageComponent {
  @Input() alt: string | null = null;
  @Input() publicId: string;
  @Input() fetchFormat: FetchFormat;
  @Input() width: StringNumber;
  @Input() height: StringNumber;
  @Input() crop: Crop;
  @Input() gravity: string;
  @Input() quality: Quality;
  @Input() radius: StringNumber;
  @Input() angle: string;
  @Input() effect: string;
  @Input() opacity: StringNumber;
  @Input() border: string;
  @Input() background: Background;
  @Input() defaultImage: string;
  @Input() zoom: StringNumber;
  @Input() aspectRatio: StringNumber;
  @Input() dpr: DPR;
  @Input() color: string;
  @Input() flags: string | string[];
  @Input() sizes: string;
  @Input() loading: 'lazy' | 'eager' = 'eager';
  /** Array of widths for a srcset, example: [400,800] will yield 400w and 800w srcset */
  @Input() cldSrcSet: number[];
  @ContentChildren(CloudinaryTransformationDirective)
  transformations: QueryList<CloudinaryTransformationDirective>;

  constructor(private el: ElementRef, private service: TinyCloudinaryService) {}

  getUrl(widthOverride?: string) {
    const transforms = this.getTransformationsAttrs();
    const componentTransforms: Transformation = {
      fetchFormat: this.fetchFormat,
      width: widthOverride ? widthOverride : this.width,
      height: this.height,
      crop: this.crop,
      gravity: this.gravity,
      quality: this.quality,
      radius: this.radius,
      angle: this.angle,
      effect: this.effect,
      opacity: this.opacity,
      border: this.border,
      background: this.background,
      defaultImage: this.defaultImage,
      zoom: this.zoom,
      aspectRatio: this.aspectRatio,
      dpr: this.dpr,
      color: this.color,
      flags: this.flags,
    };
    // Removed undefined keys
    Object.keys(componentTransforms).forEach(
      (key) =>
        componentTransforms[key] === undefined &&
        delete componentTransforms[key],
    );
    return this.service.buildUrl(
      this.publicId,
      componentTransforms,
      transforms,
    );
  }

  getSrcSet() {
    const images = this.cldSrcSet.map(
      (width) => this.getUrl(width.toString()) + ' ' + width.toString() + 'w',
    );
    return images.join();
  }

  private getTransformationsAttrs(): Transformation[] {
    const attrNodes = this.transformations.map((transformation) =>
      transformation.getAttributes(),
    );
    const transforms = attrNodes
      .map((attrNode) => {
        const numAttrs = attrNode.length;
        const result: Attr[] = [];
        for (let i = 1; i < numAttrs; i++) {
          result.push(attrNode.item(i));
        }
        return result;
      })
      .map((attr) => attr.map((a) => ({ key: a.name, value: a.value })));

    return transforms.map((transform) => {
      const result: Transformation = {};
      transform.map((item) => {
        result[item.key] = item.value;
      });
      return result;
    });
  }
}
