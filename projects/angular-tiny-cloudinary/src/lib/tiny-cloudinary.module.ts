import { NgModule, ModuleWithProviders } from "@angular/core";
import { CommonModule } from "@angular/common";
import { TinyCloudinaryConfiguration } from "./interfaces";
import { CloudinaryImageComponent } from "./cloudinary-image.component";
import { CloudinaryTransformationDirective } from "./cloudinary-transformation.directive";
import { CloudinaryVideoComponent } from "./cloudinary-video.component";

@NgModule({
  declarations: [
    CloudinaryImageComponent,
    CloudinaryVideoComponent,
    CloudinaryTransformationDirective
  ],
  imports: [CommonModule],
  exports: [
    CloudinaryImageComponent,
    CloudinaryVideoComponent,
    CloudinaryTransformationDirective
  ]
})
export class TinyCloudinaryModule {
  static forRoot(config: TinyCloudinaryConfiguration): ModuleWithProviders {
    return {
      ngModule: TinyCloudinaryModule,
      providers: [{ provide: "TinyCloudinaryConfiguration", useValue: config }]
    };
  }
}
