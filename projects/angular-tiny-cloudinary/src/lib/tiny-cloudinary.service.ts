import { Injectable, Inject } from "@angular/core";
import { TinyCloudinaryConfiguration, Transformation } from "./interfaces";

@Injectable({
  providedIn: "root"
})
export class TinyCloudinaryService {
  constructor(
    @Inject("TinyCloudinaryConfiguration")
    private config: TinyCloudinaryConfiguration
  ) {}

  getDefaultTransformations() {
    const result: Transformation = {};
    if (this.config.defaults) {
      if (this.config.defaults.f_auto) {
        result.fetchFormat = "auto";
      }
      if (this.config.defaults.q_auto) {
        result.quality = "auto";
      }
      if (this.config.defaults.defaultImage) {
        result.defaultImage = this.config.defaults.defaultImage;
      }
      if (this.config.defaults.dpr_auto) {
        result.dpr = "auto";
      }
      if (this.config.defaults.w_auto) {
        result.width = "auto";
      }
    }
    return result;
  }

  transformationToString(transformation: Transformation) {
    const result: string[] = [];
    if (transformation.fetchFormat) {
      if (transformation.fetchFormat === "auto") {
        result.push("f_auto");
      }
    }
    if (transformation.quality) {
      result.push("q_" + transformation.quality);
    }
    if (transformation.width) {
      result.push("w_" + transformation.width);
    }
    if (transformation.height) {
      result.push("h_" + transformation.height);
    }
    if (transformation.crop) {
      result.push("c_" + transformation.crop);
    }
    if (transformation.gravity) {
      result.push("g_" + transformation.gravity);
    }
    if (transformation.radius) {
      result.push("r_" + transformation.radius);
    }
    if (transformation.angle) {
      result.push("a_" + transformation.angle);
    }
    if (transformation.effect) {
      result.push("e_" + transformation.effect);
    }
    if (transformation.opacity) {
      result.push("o_" + transformation.opacity);
    }
    if (transformation.border) {
      result.push("bo_" + transformation.border);
    }
    if (transformation.background) {
      if (transformation.background === "auto") {
        result.push("b_auto");
      }
      result.push("b_rgb:" + transformation.background.replace("#", ""));
    }
    if (transformation.defaultImage) {
      result.push("d_" + transformation.defaultImage);
    }
    if (transformation.zoom) {
      result.push("z_" + transformation.zoom);
    }
    if (transformation.aspectRatio) {
      result.push("ar_" + transformation.aspectRatio);
    }
    if (transformation.dpr) {
      result.push("dpr_" + transformation.dpr);
    }
    if (transformation.color) {
      result.push("co_rgb_" + transformation.dpr.replace("#", ""));
    }
    if (transformation.flags) {
      if (transformation.flags instanceof Array) {
        result.push("fl_" + transformation.flags.join("."));
      } else {
        result.push("fl_" + transformation.flags);
      }
    }
    return result.join();
  }

  getExtension(componentTransformation: Transformation) {
    if (
      componentTransformation.fetchFormat &&
      componentTransformation.fetchFormat !== "auto"
    ) {
      return "." + componentTransformation.fetchFormat;
    }
    return "";
  }

  buildUrl(
    image: string,
    componentTransformation?: Transformation,
    transformations?: Transformation[],
    isVideo?: boolean
  ) {
    const domain = "https://res.cloudinary.com";
    const cloudName = this.config.cloudName;

    const combinedComponentTransformation: Transformation = {
      ...this.getDefaultTransformations(),
      ...componentTransformation
    };

    let allTransforms = [combinedComponentTransformation];
    if (transformations) {
      allTransforms = allTransforms.concat(transformations);
    }

    const transformsStringArray = allTransforms.map(transform =>
      this.transformationToString(transform)
    );
    let transforms = transformsStringArray.join("/");
    if (transforms) {
      transforms = transforms + "/";
    }
    const ext = this.getExtension(combinedComponentTransformation);
    const type = isVideo ? "video" : "image";
    return `${domain}/${cloudName}/${type}/upload/${transforms}${image}${ext}`;
  }
}
